package com.twu.market;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.twuc.market.MarketApplication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

@SpringBootTest(classes = {MarketApplication.class})
@AutoConfigureMockMvc
@Transactional
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public abstract class ApiTestBase {
    private final ObjectMapper mapper;
    @Autowired protected MockMvc mockMvc;

    protected ApiTestBase() {
        mapper = new ObjectMapper();
        mapper.registerModule(new JavaTimeModule());
        mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
    }

    protected String serialize(Object obj) throws Exception {
        // 这个方法可以在测试中用来正确的将含有 Java 8 时间 field 的 Object 序列化为 JSON。
        return mapper.writeValueAsString(obj);
    }

    public ObjectMapper getMapper() {
        return mapper;
    }
}
