package com.twu.market.web;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.twu.market.ApiTestBase;
import com.twuc.market.contract.CreateGoodRequest;
import com.twuc.market.contract.CreateGoodResponse;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class GoodControllerTest extends ApiTestBase {

    private static final String URL_EXAMPLE =
            "https://img11.360buyimg.com/n1/jfs/t4705/83/2924377281/70031/aed9bbd3/58f5629dN79b4406c.jpg";

    private void addGood(int count) throws Exception {
        for (int i = 0; i < count; i++) {
            CreateGoodRequest goodRequest = new CreateGoodRequest("Cola", 3.0F, "can", URL_EXAMPLE);

            mockMvc.perform(
                    post("/api/goods")
                            .contentType(MediaType.APPLICATION_JSON_UTF8)
                            .content(serialize(goodRequest)));
        }
    }

    @Test
    void should_add_good() throws Exception {
        CreateGoodRequest goodRequest = new CreateGoodRequest("Cola", 3.0F, "can", URL_EXAMPLE);

        mockMvc.perform(
                        post("/api/goods")
                                .contentType(MediaType.APPLICATION_JSON_UTF8)
                                .content(serialize(goodRequest)))
                .andExpect(status().isCreated())
                .andExpect(header().string("Location", "/api/staffs/1"));
    }

    @Test
    void should_get_all_goods() throws Exception {
        int count = 3;
        addGood(count);

        String content =
                mockMvc.perform(get("/api/goods")).andReturn().getResponse().getContentAsString();

        ObjectMapper mapper = getMapper();
        List<CreateGoodResponse> goods =
                mapper.readValue(content, new TypeReference<List<CreateGoodResponse>>() {});

        CreateGoodResponse firstGoodResponse = goods.get(0);
        CreateGoodResponse lastGoodResponse = goods.get(goods.size() - 1);

        System.out.println(content);

        assertSame(1L, firstGoodResponse.getId());
        assertEquals(URL_EXAMPLE, firstGoodResponse.getImageUrl());

        assertEquals(Long.valueOf(count), lastGoodResponse.getId());
        assertEquals(URL_EXAMPLE, lastGoodResponse.getImageUrl());
    }
}
