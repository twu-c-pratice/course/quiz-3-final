package com.twuc.market.contract;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class CreateGoodResponse {

    @NotNull private Long id;

    @NotNull
    @Size(min = 0, max = 32)
    private String name;

    @NotNull
    @Min(0)
    private float price;

    @NotNull
    @Size(min = 0, max = 16)
    private String unit;

    @NotNull
    @Size(min = 0, max = 300)
    private String imageUrl;

    public CreateGoodResponse() {}

    public CreateGoodResponse(
            @NotNull Long id,
            @NotNull @Size(min = 0, max = 32) String name,
            @NotNull @Min(0) float price,
            @NotNull @Size(min = 0, max = 16) String unit,
            @NotNull @Size(min = 0, max = 300) String imageUrl) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.unit = unit;
        this.imageUrl = imageUrl;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
