package com.twuc.market.contract;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class CreateGoodRequest {

    @NotNull
    @Size(min = 0, max = 32)
    private String name;

    @NotNull
    @Min(0)
    private float price;

    @NotNull
    @Size(min = 0, max = 16)
    private String unit;

    @NotNull
    @Size(min = 0, max = 300)
    private String imageUrl;

    public CreateGoodRequest() {}

    public CreateGoodRequest(
            @NotNull @Size(min = 0, max = 32) String name,
            @NotNull @Min(0) float price,
            @NotNull @Size(min = 0, max = 16) String unit,
            @NotNull @Size(min = 0, max = 300) String imageUrl) {
        this.name = name;
        this.price = price;
        this.unit = unit;
        this.imageUrl = imageUrl;
    }

    public String getName() {
        return name;
    }

    public float getPrice() {
        return price;
    }

    public String getUnit() {
        return unit;
    }

    public String getImageUrl() {
        return imageUrl;
    }
}
