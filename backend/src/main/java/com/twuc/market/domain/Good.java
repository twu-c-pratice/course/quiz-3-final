package com.twuc.market.domain;

import javax.persistence.*;

@Entity
public class Good {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(length = 32)
    private String name;

    @Column private float price;

    @Column(length = 16)
    private String unit;

    @Column(length = 300)
    private String imageUrl;

    public Good() {}

    public Good(String name, float price, String unit, String imageUrl) {
        this.name = name;
        this.price = price;
        this.unit = unit;
        this.imageUrl = imageUrl;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public float getPrice() {
        return price;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getUnit() {
        return unit;
    }
}
