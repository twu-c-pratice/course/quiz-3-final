package com.twuc.market.web;

import com.twuc.market.contract.CreateGoodRequest;
import com.twuc.market.domain.Good;
import com.twuc.market.domain.GoodRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/goods")
@CrossOrigin(
        origins = "*",
        allowedHeaders = "*",
        exposedHeaders = {"GET", "POST"})
public class GoodController {
    @Autowired private GoodRepository goodRepo;

    @PostMapping
    public ResponseEntity addGood(@RequestBody @Valid CreateGoodRequest goodRequest) {

        Good good =
                goodRepo.save(
                        new Good(
                                goodRequest.getName(),
                                goodRequest.getPrice(),
                                goodRequest.getUnit(),
                                goodRequest.getImageUrl()));

        return ResponseEntity.status(HttpStatus.CREATED)
                .header("Location", String.format("/api/staffs/%d", good.getId()))
                .build();
    }

    @GetMapping
    public ResponseEntity getAllGoods() {
        List<Good> goods = goodRepo.findAll();
        return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON_UTF8).body(goods);
    }
}
