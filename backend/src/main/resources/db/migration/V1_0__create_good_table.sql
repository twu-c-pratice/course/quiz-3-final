CREATE TABLE good
(
    id        bigint       NOT NULL AUTO_INCREMENT PRIMARY KEY,
    name      varchar(32)  NOT NULL,
    price     float        NOT NULL,
    unit      varchar(16)  NOT NULL,
    image_url varchar(300) NOT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 1;