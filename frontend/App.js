import React from 'react';
import Header from "./src/components/Header";

import './App.less';
import Content from "./src/components/Content";
import {Provider} from "react-redux";
import store from "./store";
import {BrowserRouter} from "react-router-dom";

const App = () => (
  <Provider store={store}>
    <BrowserRouter>
      <div className='app'>
        <Header/>
        <Content/>
      </div>
    </BrowserRouter>
  </Provider>
);

export default App;