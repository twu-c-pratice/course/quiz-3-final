const initState = {goods: []};
import {GET_ALL_GOODS} from "../actions/good";

function goodReducer(state = initState, action) {
  switch (action.type) {
    case GET_ALL_GOODS:
      return {
        ...state,
        goods: action.payload
      };
    default:
      return state;
  }
}

export default goodReducer;
