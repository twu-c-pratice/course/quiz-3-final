import {combineReducers} from "redux";
import goodReducer from "./good";

const reduces = combineReducers({
  good: goodReducer
});

export default reduces;