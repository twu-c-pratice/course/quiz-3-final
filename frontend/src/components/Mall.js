import React, {Component} from 'react';
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {getAllGoods} from "../actions/good";
import Good from "./Good";

class Mall extends Component {

  constructor(props, context) {
    super(props, context);
    this.renderAllGoods = this.renderAllGoods.bind(this);
  }

  componentDidMount() {
    this.props.getAllGoods();
  }

  renderAllGoods() {
    const goods = Array.from(this.props.goods);
    return goods.map(good => <Good key={good.id} good={good}/>);
  }

  render() {
    return (
      <div className="mall">
        {this.renderAllGoods()}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  goods: state.good.goods
});

const mapDispatchToProps = dispatch => bindActionCreators({
  getAllGoods
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Mall);
