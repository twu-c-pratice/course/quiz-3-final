import React, {Component} from 'react';

class AddGood extends Component {

  constructor(props, context) {
    super(props, context);
    this.state = {price: 0};
    this.handleInputChange = this.handleInputChange.bind(this);
    this.setState = this.setState.bind(this);
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }

  addGood() {

  }

  render() {
    return (
      <div>
        <h1>添加商品</h1>
        <form>
          <label for="name">
            <p>* 名称</p>
            <input required type="text" name="name" onChange={this.handleInputChange} placeholder="名称"/>
          </label>
          <label for="price">
            <p>* 价格</p>
            <input required type="text" name="price" onChange={this.handleInputChange} placeholder="名称"/>
          </label>
          <label for="unit">
            <p>* 单位</p>
            <input required type="text" name="unit" onChange={this.handleInputChange} placeholder="名称"/>
          </label>
          <label for="image_url">
            <p>* 图片</p>
            <input required type="text" name="image_url" onChange={this.handleInputChange} placeholder="名称"/>
          </label>
          <button type="submit" onClick={this.addGood}>提交</button>
        </form>
      </div>
    );
  }
}

export default AddGood;