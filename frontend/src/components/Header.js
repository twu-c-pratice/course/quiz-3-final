import React from 'react';
import '../style/Header.less'
import {NavLink} from "react-router-dom";

const Header = () => (
  <header className="header">
    <ul>
      <li><NavLink exact activeClassName="active" to='/'>商城</NavLink></li>
      <li><NavLink exact activeClassName="active" to='/order'>订单</NavLink></li>
      <li><NavLink exact activeClassName="active" to='add-good'>添加商品</NavLink></li>
    </ul>
  </header>
);

export default Header;
