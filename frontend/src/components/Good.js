import React from 'react';
import '../style/Good.less';

const Good = (props) => (
  <div className="good">
    <img src={props.good.imageUrl} alt={props.good.name}/>
    <p className="good-name"> {props.good.name}</p>
    <p className="good-info">price：{props.good.price} yuan / {props.good.unit} </p>
  </div>
);

export default Good;