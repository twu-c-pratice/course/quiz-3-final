import React from 'react';
import Mall from "./Mall";
import Order from "./Order";
import AddGood from "./AddGood";
import {Route, Switch} from "react-router-dom";

const Content = () => (
  <main className="content">
    <Switch>
      <Route exact path="/" component={Mall}/>
      <Route exact path="/order" component={Order}/>
      <Route exact path="/add-good" component={AddGood}/>
    </Switch>
  </main>
);

export default Content;