export const GET_ALL_GOODS = "GET_ALL_GOODS";

const getAllGoods = () => dispatch => (
  fetch('http://localhost:8080/api/goods')
    .then(response => response.json())
    .then(result => dispatch({
      type: GET_ALL_GOODS,
      payload: result
    }))
);

export {getAllGoods};